//
//  GameResult.h
//  Matchismo2
//
//  Created by Victor Engel on 3/19/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameResult : NSObject

@property (readwrite, nonatomic) NSString *gameName;
//@property (readonly, nonatomic) NSString *gameName;
@property (readonly, nonatomic) NSDate *start;
@property (readonly, nonatomic) NSDate *end;
@property (readonly, nonatomic) NSTimeInterval duration;
@property (nonatomic) int score;

+(NSArray *)allGameResults;

- (NSComparisonResult)compareStart:(GameResult *)otherObject;
- (NSComparisonResult)compareDuration:(GameResult *)otherObject;
- (NSComparisonResult)compareScore:(GameResult *)otherObject;
@end
