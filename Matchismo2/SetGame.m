//
//  SetGame.m
//  Matchismo
//
//  Created by Victor Engel on 2/7/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//
// New code to modify, copied from CardMatchingGame

#import "SetGame.h"
#import "SetCardDeck.h"
#import "SetCard.h"

@interface SetGame()
@property (strong, nonatomic) NSMutableArray *cards;
@property (strong, nonatomic) NSMutableArray *otherCards;
@property (readwrite, nonatomic) int score;
@property (nonatomic, readwrite) NSString *flipResult;
@end

@implementation SetGame

-(NSMutableArray *)cards
{
   if (!_cards) _cards = [[NSMutableArray alloc] init];
   return _cards;
}
-(NSUInteger)cardCount
{
   return [self.cards count];
}
-(NSMutableArray *)otherCards
{
   if (!_otherCards) _otherCards = [[NSMutableArray alloc] init];
   return _otherCards;
}

-(id)initWithCardCount:(NSUInteger)cardCount usingDeck:(Deck *)deck
{
   self = [super init];
   if (self) {
      for (int i=0; i<cardCount; i++) {
         Card *card = [deck drawRandomCard];
         if (!card) {
            self = nil; //This can happen if there are not enough cards in the deck.
            break;
         } else {
            self.cards[i] = card;
         }
      }
   }
   self.gameMode = 3;
   self.flipResult = [NSString stringWithFormat:@"Total sets: %d",[self setsShowing]];
   return self;
}

-(Card *)cardAtIndex:(NSUInteger)index
{
   return (index < [self.cards count]) ? self.cards[index] : nil;
}

-(void)flipCardAtIndex:(NSUInteger)index gameMode:(NSUInteger)gameMode
{
   //gameMode indicates number of cards to match.
   Card *card = [self cardAtIndex:index];
   BOOL cardIsPlayable = !card.isUnplayable;
   //The card is already face up, so it just needs to be turned back over.
   if (card.faceUp && cardIsPlayable) {
      //self.flipResult = [NSString stringWithFormat:@"%@ flipped",card.contents];
      card.faceUp = !card.faceUp;
      [self.otherCards removeObjectIdenticalTo:card];
   } else {
      if (cardIsPlayable) {
         //self.flipResult = [NSString stringWithFormat:@"%@ flipped",card.contents];
         //The card is face down, so it needs to be turned face up.
         card.faceUp = YES;
         //If turning over the new card results in gameMode cards turned up, evaluate the match.
         int totalCards = 1 + [self.otherCards count];
         if (totalCards == gameMode) {
            int matchScore = [card match:self.otherCards];
            // There are 4 attributes to a card. There is one point awarded for each attribute, so a score of 4 is a match.
            if (matchScore == 4) {
               [self.otherCards addObject:card];
               [self deleteCards:self.otherCards];
               self.otherCards = nil;
               //self.score += 1;
               if ([self cardCount] < 24) {
                  int divisor = [self cardCount];
                  if (divisor > 3) {
                     divisor -= 3;
                     self.score += 2520 / [self cardCount];
                  } else {
                     self.score += 5040; //There is a bonus for clearing the cards.
                  }
               }
               // There is guaranteed to be a set with 21 cards or more, so don't award any points if more than 21 cards is dealt.
            } else {
               //self.flipResult = @"No match - flip oldest card";
               //There was no score. Assess a penalty and turn oldest card over.
               //If the result of the match is 0, then turn the oldest card face down.
               Card *oldestCard = self.otherCards[0];
               oldestCard.faceUp = NO;
               [self.otherCards removeObjectAtIndex:0];
               //Now add the current card to the otherCards array.
               [self.otherCards addObject:card];
               // self.score -= 1;
               if ([self cardCount] >=24) {
                  self.score -= 5040;
               } else {
                  self.score -= 2 * 2520 / [self cardCount];
               }
            }
         } else {
            [self.otherCards addObject:card];
         }
      }
   }
   self.flipResult = [NSString stringWithFormat:@"Total sets: %d",[self setsShowing]];
}
-(void)dealThreeMore
{
   for (int i=0; i<3; i++) {
      Card *card = [self.deck drawRandomCard];
      if (card) [self.cards addObject:card];
   }
   self.flipResult = [NSString stringWithFormat:@"Total sets: %d",[self setsShowing]];
}
-(int)setsShowing
{
   //Returns the total number of sets showing. If a single card can be involved in more than one set, each set is included in the count.
   int runningCount = 0;
   SetCard *thirdCard;
   for (SetCard *card in self.cards) {
      //For each card, look at each other card.
      for (SetCard *secondCard in self.cards) {
         if ([self.cards indexOfObject:secondCard] > [self.cards indexOfObject:card]) {
            //if (card != secondCard) {
            //We are now looking at two DIFFERENT cards. Given any two cards, there is only one other card that results in a set.
            //NSLog(@"Comparing card %@ to card %@",card,secondCard);
            thirdCard = [[SetCard alloc] init];
            BOOL isSet = NO;
            thirdCard.number  = [SetCard numberMatching: card.number  and:secondCard.number];
            thirdCard.symbol  = [SetCard symbolMatching: card.symbol  and:secondCard.symbol];
            thirdCard.shading = [SetCard shadingMatching:card.shading and:secondCard.shading];
            thirdCard.color   = [SetCard colorMatching:  card.color   and:secondCard.color];
            SetCard *searchCard;
            for (searchCard in self.cards) {
               if ([thirdCard.number isEqualToString:searchCard.number])
               {
                  if ([thirdCard.symbol isEqualToString:searchCard.symbol])
                  {
                     if ([thirdCard.shading isEqualToString:searchCard.shading])
                     {
                        if ([thirdCard.color isEqualToString:searchCard.color])
                        {
                           isSet = YES;
                           break; //If we found a match, we can discontinue the loop
                        }
                     }
                  }
               }
            }
            if ([self.cards indexOfObject:searchCard] > [self.cards indexOfObject:secondCard]) {
               if (isSet) {
                  //NSLog(@"Found third card is %@",thirdCard);
                  runningCount ++;
               }
            }
         }
      }
   }
   return runningCount;
}
@end


