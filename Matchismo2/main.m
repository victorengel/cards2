//
//  main.m
//  Matchismo2
//
//  Created by Victor Engel on 3/16/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MatchismoAppDelegate.h"

int main(int argc, char *argv[])
{
   @autoreleasepool {
       return UIApplicationMain(argc, argv, nil, NSStringFromClass([MatchismoAppDelegate class]));
   }
}
