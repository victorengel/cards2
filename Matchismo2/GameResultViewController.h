//
//  GameResultViewController.h
//  Matchismo2
//
//  Created by Victor Engel on 3/19/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameResultViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *display;

- (IBAction)recent:(id)sender;
- (IBAction)score:(id)sender;
- (IBAction)speed:(id)sender;


@end
