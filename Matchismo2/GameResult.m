//
//  GameResult.m
//  Matchismo2
//
//  Created by Victor Engel on 3/19/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "GameResult.h"

@interface GameResult()
@property (readwrite, nonatomic) NSDate *start;//readonly in .h so make readwrite here.
@property (readwrite, nonatomic) NSDate *end;
@end

@implementation GameResult

#define ALL_RESULTS_KEY @"GameResult_ALL"
#define GAMENAME_KEY @"GameName"
#define START_KEY @"StartDate"
#define END_KEY @"EndDate"
#define SCORE_KEY @"Score"
-(id)init
{
   self = [super init];
   if (self) {
      _start = [NSDate date];//now
      _end = _start;         //just so _end is always set
                             //Don't call setters/getters in initializers, especially the designated initializer.
      _gameName = @"";
   }
   return self;
}

-(NSTimeInterval) duration
{
   return [self.end timeIntervalSinceDate:self.start];
}

-(void)setGameName:(NSString *)gameName
{
   _gameName = gameName;
   [self synchronize];
}

-(void)setScore:(int)score
{
   _score = score;
   self.end = [NSDate date];//now
   [self synchronize];
}

-(void)synchronize
{
   //General procedure to modify existing item in NSUserDefaults:
   // 1. Take old value out.
   // 2. Mutable copy
   // 3. Modify the copy.
   // 4. Put it back in
   // 5. synchronize
   NSMutableDictionary *mutableGameResultsFromUserDefaults = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] mutableCopy];
   if (!mutableGameResultsFromUserDefaults) {
      mutableGameResultsFromUserDefaults = [[NSMutableDictionary alloc]init];
   }
   //Key the games by when they start -- that is unique.
   mutableGameResultsFromUserDefaults[[self.start description]] = [self asPropertyList];
   [[NSUserDefaults standardUserDefaults] setObject:mutableGameResultsFromUserDefaults forKey:ALL_RESULTS_KEY];
   [[NSUserDefaults standardUserDefaults] synchronize];
}
-(id)asPropertyList
{
   //Create a dictionary of self to store in the property list.
   return @{GAMENAME_KEY : self.gameName, START_KEY : self.start, END_KEY : self.end, SCORE_KEY : @(self.score)};
}

-(id)initFromPropertyList:(id)plist
//Inverse of asPropertyList
{
   self = [self init];
   if (self) {
      if ([plist isKindOfClass:[NSDictionary class]]) {
         NSDictionary *resultDictionary = (NSDictionary *)plist;
         _gameName = resultDictionary[GAMENAME_KEY];
         _start = resultDictionary[START_KEY];
         _end = resultDictionary[END_KEY];
         _score = [resultDictionary[SCORE_KEY] intValue];
         if (!_start || !_end) {
            self = nil;
         }
      }
   }
   return self;
}
+(NSArray *)allGameResults
//Returns an array of all game results.
{
   NSMutableArray *allGameResults = [[NSMutableArray alloc] init];
   for (id plist in [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] allValues]) {
      GameResult *result = [[GameResult alloc] initFromPropertyList:plist];
      [allGameResults addObject:result];
   }
   return allGameResults;
}

- (NSComparisonResult)compareStart:(GameResult *)otherObject
{
   return [self.start compare:otherObject.start];
}
- (NSComparisonResult)compareStart2:(GameResult *)otherObject
{
   return [otherObject.start compare:self.start];
}

- (NSComparisonResult)compareDuration:(GameResult *)otherObject
{
   return [@(self.duration) compare:@(otherObject.duration)];
}
- (NSComparisonResult)compareDuration2:(GameResult *)otherObject
{
   return [@(otherObject.duration) compare:@(self.duration)];
}

- (NSComparisonResult)compareScore:(GameResult *)otherObject
{
   return [@(self.score) compare:@(otherObject.score)];
}
- (NSComparisonResult)compareScore2:(GameResult *)otherObject
{
   return [@(otherObject.score) compare:@(self.score)];
}

@end
