//
//  CardGameViewController.h
//  Matchismo
//
//  Created by Victor Engel on 1/31/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Deck.h"
#import "GameResult.h"

@interface CardGameViewController : UIViewController

@property (nonatomic) NSUInteger startingCardCount; //abstract
@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *flipResultLabel;
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
//Following line declares gameResult. Not sure why it's not visible in SetGameViewController.
@property (strong, nonatomic) GameResult *gameResult;//game result of the current game

-(Deck *)createDeck; //abstract
-(void)updateCell: (UICollectionViewCell *)cell usingCard:(Card *)card; //abstract
-(void)updateUI;
- (IBAction)deal:(UIButton *)sender;
-(void)setFlipCount:(int)flipCount;

@end
