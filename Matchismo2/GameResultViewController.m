//
//  GameResultViewController.m
//  Matchismo2
//
//  Created by Victor Engel on 3/19/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "GameResultViewController.h"
#import "GameResult.h"

@interface GameResultViewController ()

typedef enum
{
   unsorted,
   sortedByDate,
   sortedByDate2,
   sortedByDuration,
   sortedByDuration2,
   sortedByScore,
   sortedByScore2
} sortingMode;

@property (weak, nonatomic) IBOutlet UITextView *resultList;
@property (nonatomic) sortingMode currentSortingMode;

@end

@implementation GameResultViewController
@synthesize display;

-(void)updateUI
{
   NSString *displayText = @"";
   /*
   for (GameResult *result in [GameResult allGameResults]) {
      displayText = [displayText stringByAppendingFormat:@"%@: Score: %d (%@, %0g)\n",result.gameName ,result.score, result.end, round(result.duration)];
   }
   self.resultList.text = displayText;
    */
   NSArray *allGameResults = [GameResult allGameResults];
   NSArray *sortedResults;
   
   switch ([self currentSortingMode])
   {
      case sortedByDate:
         sortedResults = [allGameResults sortedArrayUsingSelector:@selector(compareStart:)];
         break;
         
      case sortedByDate2:
         sortedResults = [allGameResults sortedArrayUsingSelector:@selector(compareStart2:)];
         break;
         
      case sortedByDuration:
         sortedResults = [allGameResults sortedArrayUsingSelector:@selector(compareDuration:)];
         break;
         
      case sortedByDuration2:
         sortedResults = [allGameResults sortedArrayUsingSelector:@selector(compareDuration2:)];
         break;
         
      case sortedByScore:
         sortedResults = [allGameResults sortedArrayUsingSelector:@selector(compareScore:)];
         break;
         
      case sortedByScore2:
         sortedResults = [allGameResults sortedArrayUsingSelector:@selector(compareScore2:)];
         break;
         
      case unsorted:
         // no sorting to do in this case
         sortedResults = allGameResults;
         break;
   }
   
   for (GameResult *result in sortedResults)
   {
      //displayText = [displayText stringByAppendingFormat:@"Score: %d (%@, %0g)\n", result.score, result.end, round(result.duration)];
      displayText = [displayText stringByAppendingFormat:@"%@: Score: %d (%@, %0g)\n",result.gameName ,result.score, result.end, round(result.duration)];
   }
   
   self.display.text = displayText;
}
-(void)viewWillAppear:(BOOL)animated
{
   [super viewWillAppear:animated];
   [self updateUI];
}
-(void)setup
{
   
}
-(void)awakeFromNib
{
   [self setup];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
   self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
   [self setup];
   return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)recent:(UIButton *)sender {
   if (sender.tag == 2) {
      self.currentSortingMode = sortedByDate;
   } else {
      self.currentSortingMode = sortedByDate2;
   }
   
   [self updateUI];

}

- (IBAction)score:(UIButton *)sender {
   if (sender.tag == 2) {
      self.currentSortingMode = sortedByScore;
   } else {
      self.currentSortingMode = sortedByScore2;
   }
   
   [self updateUI];
}

- (IBAction)speed:(UIButton *)sender {
   if (sender.tag == 2) {
      self.currentSortingMode = sortedByDuration;
   } else {
      self.currentSortingMode = sortedByDuration2;
   }
   
   [self updateUI];
}
@end
